## Abstract
---

In computer graphics, turtle graphics are vector graphics using a relative cursor (the "turtle") upon a Cartesian plane. Turtle graphics is a key feature of the Logo programming language.
The turtle has three attributes: a location, an orientation (or direction), and a pen.
The pen, too, has attributes: color, width, and on/off state. The turtle moves with commands that are relative to its own position,
such as "move forward 10 spaces" and "turn left 90 degrees". A student could understand (and predict and reason about) the turtle's motion
by imagining what they would do if they were the turtle. Seymour Papert called this "body syntonic" reasoning. The goal is to create a small
language that kids can use to program the turtle. Currently drawing turtle literally is not supported.


## Input
---
To draw a square, below commands are given to the program.

```
#side 50
repeat 4
    move side
    turn 90
end
```