package visitor;

import interpreter.*;

/*
 * Visitor interface to be implemented by all visitor classes.
 */
public interface IVisitor {

	public void visit(MoveTurtleExpression expression);

	public void visit(TurnTurtleExpression expression);

	public void visit(RepeatExpression expression);

	public void visit(PenDownExpression expression);

	public void visit(PenUpExpression expression);
	
}
