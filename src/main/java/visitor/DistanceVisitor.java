package visitor;

import interpreter.*;
import TurtleImplmentation.Point;
import TurtleImplmentation.Turtle;

/*
 * Visitor Implementation for calculating overall distance covered by the turtle.
 */
public class DistanceVisitor implements IVisitor {
	
	private int distanceCovered;
	private Turtle turtle;
	
	public DistanceVisitor(Turtle turtle) {
		this.turtle = turtle;
		this.distanceCovered = 0;
	}

	/*
	 * If the Expression type is move, then increment the distance by calculating
	 * distance between turtle's former and new position.
	 */
	public void visit(MoveTurtleExpression expression) {

		Point location = new Point(turtle.getLocation().getX(),turtle.getLocation().getY());
		expression.interpret(turtle);
		this.distanceCovered += location.distanceFrom(turtle.getLocation());
	}

	public void visit(TurnTurtleExpression expression) {

		expression.interpret(turtle);
	}

	/*
	 * If the expression type is repeat, increment the distance by calculating
	 * distance between turtle's former and new position for each expression type.
	 */
	public void visit(RepeatExpression expression) {

		for(int i=0;i<expression.getRepetitionCount();i++){
			for(IExpression exp: expression.getInstructions() )
			{
				exp.accept(this);
			}
		}
	}

	public void visit(PenDownExpression expression) {

		expression.interpret(turtle);
	}

	public void visit(PenUpExpression expression) {

		expression.interpret(turtle);
	}

	/*
	 * Return total distance covered by this visitor.
	 */
	public double getDistanceCovered() {
		
		return distanceCovered;
	}


}
