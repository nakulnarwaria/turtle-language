package visitor;

import interpreter.*;
import TurtleImplmentation.Point;
import TurtleImplmentation.Turtle;
import memento.TurtleCareTaker;
import memento.TurtleMemento;
import memento.TurtleOriginator;

import java.util.Iterator;

/*
 * Step Visitor implementation to return mementos at each execution of expression.
 */
public class StepVisitor implements IVisitor{

	private Turtle turtle;
	private TurtleCareTaker careTaker = new TurtleCareTaker();
	
	
	public StepVisitor(Turtle turtle){
		this.turtle = turtle;
	}
	
	
	public void visit(MoveTurtleExpression expression) {

		expression.interpret(turtle);
		this.addTurtleMemento(turtle);
	}


	public void visit(TurnTurtleExpression expression) {

		expression.interpret(turtle);
		this.addTurtleMemento(turtle);
	}


	/*
	 * If expression type is repeat, execute all expression stored inside the repeat expression,
	 * and store mementos simultaneously.
	 */
	public void visit(RepeatExpression expression) {

		for(int i=0;i<expression.getRepetitionCount();i++){
			for(IExpression exp: expression.getInstructions() )
			{
				exp.accept(this);
			}
		}
	}


	public void visit(PenDownExpression expression) {

		expression.interpret(turtle);
		this.addTurtleMemento(turtle);
	}


	public void visit(PenUpExpression expression) {

		expression.interpret(turtle);
		this.addTurtleMemento(turtle);
	}
	
	public TurtleCareTaker getTurtleCareTaker() {
		
		return this.careTaker;

	}

	public Turtle restoreFromMemento(TurtleMemento memento){
		Point location = new Point(memento.getLocation().getX(),memento.getLocation().getY());
		return new Turtle(memento.getDirection(), memento.getPenState(), location);
	}

	public Iterator iterator() {
		return careTaker.getTurtleMementoList().iterator();
	}

	/*
	 * Default implementation for adding mementos to the caretaker after each expression execution.
	 */
	private void addTurtleMemento(Turtle turtle){

		Point location = new Point(turtle.getLocation().getX(),turtle.getLocation().getY());
		TurtleOriginator originator = new TurtleOriginator(turtle.getDirection(), turtle.getPenState(), location);
		careTaker.addTurtleMemento(originator.createMemento());
	}

}
