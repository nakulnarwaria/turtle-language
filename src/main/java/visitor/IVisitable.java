package visitor;

/*
 * Visitable interface to be implemented by all expression types.
 */
public interface IVisitable {

	public void accept(IVisitor visitor);

}
