package memento;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import TurtleImplmentation.Point;

/*
 * Memento implementation to hold the specifications of current turtle.
 */
public class TurtleMemento {

	private double direction;
	private boolean penState; //false is pen down 
	private Point location;
	
	public TurtleMemento(double direction, boolean penState, Point location) {
		this.direction = direction;
		this.penState = penState;
		this.location = location;
	}
	
	
	public double getDirection() {
		return direction;
	}

	public boolean getPenState() {
		return penState;
	}

	public Point getLocation() {
		return location;
	}

	/*
	 * Returns a string that specifies the position and other details of turtle in this memento.
	 */
	public String toString() {
		NumberFormat formatter = new DecimalFormat("#0.00");
		return "X: "+formatter.format(location.getX())+" Y: "+formatter.format(location.getY())+" penState : "+penState+" direction : "+direction;
	}
}
