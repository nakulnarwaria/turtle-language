package memento;

import java.util.*;


/*
 * Caretaker class to hold and retrieve mementos
 */
public class TurtleCareTaker {
	
	private List<TurtleMemento> turtleMementos;
	
	public TurtleCareTaker() {
		
		turtleMementos = new ArrayList<TurtleMemento>();
	}
	
	public void addTurtleMemento(TurtleMemento memento) {

		this.turtleMementos.add(memento);
	}
	
	public TurtleMemento getTurtleMemento(int index) {
		
		return this.turtleMementos.get(index);
	}

	public List<TurtleMemento> getTurtleMementoList(){
		return this.turtleMementos;
	}
	
	public int size() {
		return turtleMementos.size();
	}
	
	
	

}
