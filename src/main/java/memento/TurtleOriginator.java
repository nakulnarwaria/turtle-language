package memento;

import TurtleImplmentation.Point;

/*
 * Turtle originator implementation to build mementos.
 */
public class TurtleOriginator {
	
	private double direction;
	private boolean penState; //false is pen down 
	private Point location;
	
	public TurtleOriginator(double direction, boolean penState, Point location) {
		this.direction = direction;
		this.penState = penState;
		this.location = location;
	}
	
	
	public void setDirection(double direction) {
		this.direction = direction;
	}
	
	public void setPenState(boolean penState) {
		this.penState = penState;
	}
	
	public void setLocation(Point location) {
		this.location = location;
	}
	
	public void restoreFromMemento(TurtleMemento memento) {
		
		this.direction = memento.getDirection();
		this.penState = memento.getPenState();
		this.location = memento.getLocation();
		
	}

	// Creates a new memento with current specifications.
	public TurtleMemento createMemento() {
		return new TurtleMemento(direction, penState, location);
	}

}
