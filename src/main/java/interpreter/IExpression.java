package interpreter;

import visitor.IVisitable;
import TurtleImplmentation.Turtle;


/*
 * Expression interface to be implemented by all command classes.
 */
public interface IExpression extends IVisitable {
	
	public void interpret(Turtle turtle);
	
}
