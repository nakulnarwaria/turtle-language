package interpreter;

import visitor.*;
import TurtleImplmentation.Turtle;

/*
 * Move expression accepts distance as it's parameter,
 * and perform move operation on the turtle object passed
 * to interpreter.
 */
public class MoveTurtleExpression implements IExpression{
	
	private double distance;

	public MoveTurtleExpression(double distance) {
		this.distance = distance;
	}
	
	public void interpret(Turtle turtle) {
		turtle.move(distance);
	}

	public void accept(IVisitor visitor) {

		visitor.visit(this);

	}
	

}
