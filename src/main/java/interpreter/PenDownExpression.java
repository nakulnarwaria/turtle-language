package interpreter;

import visitor.*;
import TurtleImplmentation.Turtle;

/*
 * Pen down expression perform pen down operation on
 * the turtle object passed to interpreter.
 */
public class PenDownExpression implements IExpression{
	

	public void interpret(Turtle turtle) {
		
		turtle.penDown();
	}

	public void accept(IVisitor visitor) {

		visitor.visit(this);

	}
}
