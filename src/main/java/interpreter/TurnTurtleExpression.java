package interpreter;

import visitor.*;
import TurtleImplmentation.Turtle;

/*
 * Turn expression accepts degree as it's parameter,
 * and perform turn operation on the turtle object passed
 * to interpreter.
 */
public class TurnTurtleExpression implements IExpression {
	

	private double degrees;

	public TurnTurtleExpression(double degrees) {
		this.degrees = degrees;
	}

	
	public void interpret(Turtle turtle) {

		turtle.turn(degrees);
	}

	public void accept(IVisitor visitor) {

		visitor.visit(this);

	}
	

}
