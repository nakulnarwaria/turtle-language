package interpreter;

import java.util.*;
import java.io.*;
import TurtleImplmentation.Turtle;

/*
 * Parser class to interpret instructions passed in the file,
 * and build and execute the abstract syntax trees of those instructions.
 */
public class InstructionParser  {

    private List<IExpression> abstractSyntaxTree;
    private HashMap<String,Integer> definedConstants;

    /*
     * Read file and interpret instructions to add to abstract syntax tree
     * Returns a root node for the parse tree.
     */
    public IExpression buildSyntaxTree(String fileName) throws IOException {

        File file = new File(fileName);

        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        String line;
        abstractSyntaxTree= new LinkedList<IExpression>();
        definedConstants = new HashMap<String, Integer>();
        List<IExpression> rootExpression = new LinkedList<IExpression>();

        while ((line = bufferedReader.readLine()) != null) {

            String[] instruction = line.trim().split(" ");

            if(instruction[0].charAt(0)=='#') {
                int value = Integer.parseInt(instruction[instruction.length - 1]);

                // Storing constants defined in the language by removing # and = from start and end.
                String key = instruction[0].substring(1);
                if(key.charAt(key.length()-1)=='=')
                    key = key.substring(0,key.length()-1);

                definedConstants.put(key, value);
            }
            else if (instruction[0].toLowerCase().contains("repeat")){
                String repeatInstruction;
                List<IExpression> repeatExpressions = new LinkedList<IExpression>();
                int count = Integer.parseInt(instruction[1]);
                while((repeatInstruction = bufferedReader.readLine())!=null){
                    // if end is encountered, end the repeat instructions.
                    if(repeatInstruction.toLowerCase().contains("end"))
                        break;
                    repeatExpressions.add(interpretExpression(repeatInstruction.trim().split(" ")));
                }
                rootExpression.add(new RepeatExpression(count, repeatExpressions));
            }
            else
                rootExpression.add(interpretExpression(line.split(" ")));

        }

        abstractSyntaxTree.add(new RepeatExpression(1, rootExpression));
        return new RepeatExpression(1, rootExpression);

    }

    /*
     * Interpret instructions in passed string array and return corresponding expression objects.
     */
    private IExpression interpretExpression(String[] instruction) throws IOException{

        if(instruction.length == 1){

            if(instruction[0].toLowerCase().contains("penup"))
                return new PenUpExpression();
            else if(instruction[0].toLowerCase().contains("pendown"))
                return new PenDownExpression();
            else
                throw new IOException("Illegal Command");

        }
        else if(instruction.length == 2){

            double value;
            if(instruction[1].charAt(0)=='#' || instruction[1].charAt(0)=='$')
            {
                if(definedConstants.get(instruction[1].substring(1)) != null)
                    value = definedConstants.get(instruction[1].substring(1));
                else
                    throw new IOException("Illegal Constant");
            }
            else
                value = Double.parseDouble(instruction[1]);


            if(instruction[0].toLowerCase().contains("move"))
                return new MoveTurtleExpression(value);

            else if(instruction[0].toLowerCase().contains("turn"))
                return new TurnTurtleExpression(value);
            else
                throw new IOException("Illegal Command");


        }
        return null;
    }

    /*
     * Execute expressions in the abstract syntax tree.
     */
    public void parseAbstractSyntaxTree(Turtle turtle) throws NullPointerException{
        if (abstractSyntaxTree == null)
            throw new NullPointerException();

        for (IExpression expression : abstractSyntaxTree)
            expression.interpret(turtle);

    }
}
