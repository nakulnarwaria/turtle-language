package interpreter;

import visitor.*;
import java.util.*;
import TurtleImplmentation.Turtle;

/*
 * Repeat expression accepts other expressions and repetition count
 * as it's parameter, and perform all passed expressions on the turtle
 * object passed to interpreter, repetitionCount  number of times.
 */
public class RepeatExpression implements IExpression {

	
	private List<IExpression> instructions;
	private int repetitionCount;

	public RepeatExpression(int repetitionCount, List<IExpression> instructions ) {
		this.instructions = instructions;
		this.repetitionCount = repetitionCount;
	}
	
	public void interpret(Turtle turtle) {
		
		for(int i=0;i<repetitionCount;i++) {
			for(int j=0;j<instructions.size();j++) {
				instructions.get(j).interpret(turtle);
			}
		}

	}

	public void accept(IVisitor visitor) {
		visitor.visit(this);
	}

	/*
	 * For implementation of inside expression in visitors.
	 */
	public List<IExpression> getInstructions(){
		return this.instructions;
	}

	public int getRepetitionCount(){
		return this.repetitionCount;
	}
	
}
