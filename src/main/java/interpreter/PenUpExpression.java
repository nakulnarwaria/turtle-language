package interpreter;

import visitor.*;
import TurtleImplmentation.Turtle;

/*
 * Pen up expression perform pen up operation on
 * the turtle object passed to interpreter.
 */
public class PenUpExpression implements IExpression{


	public void interpret(Turtle turtle) {
		
		turtle.penUp();
	}


	public void accept(IVisitor visitor) {

		visitor.visit(this);

	}
}
