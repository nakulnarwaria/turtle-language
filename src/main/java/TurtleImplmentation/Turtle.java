package TurtleImplmentation;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Turtle {
	
	private double direction;
	private boolean penState; //false is pen down 
	private Point location;
	private static final double PI = 3.14159;
	
	
	public Turtle(){
		this.direction = 0; //0 degrees , direction will increase counter clockwise.
		this.penState = true;
		this.location = new Point(0,0); //Assuming (0,0) as the center
	}

	//Values provided in the constructor for initial specifications
	public Turtle(double direction, boolean penState, Point location) {
		this.direction = direction;
		this.penState = penState;
		this.location = location;
	}

	public Object clone() throws CloneNotSupportedException{

		return super.clone();
	}
	
	/*
	 * Move the turtle distance units in the current direction and updates the field(s) indicating
	 * the current location of the turtle. The method does not draw on the
	 * screen.
	 */
	public void move(double distance) {
		
		double x = location.getX();
		double y = location.getY();
		x += distance * Math.cos(direction * PI / 180);
        y += distance * Math.sin(direction * PI / 180);
        location.setX(x);
        location.setY(y);
        
	}
	
	//Add “degrees” to the current heading of the turtle. 
	public void turn(double degrees) {

		degrees = degrees % 360;

		if (degrees < 0){
			degrees = 360 + degrees;
		}

		direction = (direction + degrees)%360;
	}
	
	//Lift the pen up.
	public void penUp() {
		this.penState = true;
	}
	
	//Put the pen down.
	public void penDown() {
		this.penState = false;
	}
	
	//Return true if pen is up, false if the pen is down.
	public boolean getPenState() {
		return this.penState;
	}
	
	
	//Returns the current direction of the turtle.
	public double getDirection() {
		return this.direction;
	}
	
	//Returns the current location of the turtle. 
	public Point getLocation() {
		return this.location;
	}

	//Returns a String containing Turtle specifications
	public String toString() {

		NumberFormat formatter = new DecimalFormat("#0.00");
		return "X: "+formatter.format(location.getX())+" Y: "+formatter.format(location.getY())+" penState : "+penState+" direction : "+direction;
	}
	
}
