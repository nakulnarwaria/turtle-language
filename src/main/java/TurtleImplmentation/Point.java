package TurtleImplmentation;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Point {
	
	private double x;
	private double y;

	//Default constructor with center as (0,0)
	public Point() {
		this.x = 0;
		this.y = 0;
	}

	//Creates a point with specified x and y values
	public Point(double x, double y){
		this.x = x;
		this.y = y;
	}

	/*
	 * Getters and setters for x and y
	 */
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}

	//Computes distance from another Point object.
	public double distanceFrom(Point p) {

		double xComponent = Math.pow(Math.abs(p.getX()-x),2);
		double yComponent = Math.pow(Math.abs(p.getY()-y),2);
		return Math.sqrt((xComponent + yComponent));
	}

	//Returns a String containing Point location
	public String toString() {

		NumberFormat formatter = new DecimalFormat("#0.00");
		return "X: "+formatter.format(getX())+" Y: "+formatter.format(getY());
	}

}
