import TurtleImplmentation.Turtle;
import interpreter.IExpression;
import interpreter.InstructionParser;
import org.junit.Test;
import java.io.IOException;
import static org.junit.Assert.assertEquals;


public class InterpreterTest {

    @Test
     public void testInterpreterParser() throws IOException {

        InstructionParser parser = new InstructionParser();
        String fileName = System.getProperty("user.dir")+"/src/test/java/resources/BasicFlow.txt";

        IExpression expression = parser.buildSyntaxTree(fileName);
        Turtle turtleInterpreter = new Turtle();

        expression.interpret(turtleInterpreter);

        Turtle expectedTurtle =  new Turtle();
        //File instructions
        expectedTurtle.move(10);
        expectedTurtle.turn(90);
        expectedTurtle.move(20);
        expectedTurtle.turn(-60);
        expectedTurtle.move(15);

        assertEquals(expectedTurtle.toString(), turtleInterpreter.toString());

    }

    @Test
     public void testInterpreterParserWithRepeat() throws IOException{

        InstructionParser parser = new InstructionParser();
        String fileName = System.getProperty("user.dir")+"/src/test/java/resources/RepeatWithZeroDisplacement.txt";

        IExpression expression = parser.buildSyntaxTree(fileName);
        Turtle turtleInterpreter = new Turtle();
        expression.interpret(turtleInterpreter);

        Turtle expectedTurtle =  new Turtle();
        //File instructions
        expectedTurtle.penDown(); // Repeat command in input file
        for(int i=0;i<4;i++){
           expectedTurtle.move(10);
           expectedTurtle.turn(90);
        }

        assertEquals(expectedTurtle.toString(), turtleInterpreter.toString());

    }

   @Test
    public void testInterpreterParserWithConstant() throws IOException{

       InstructionParser parser = new InstructionParser();
       String fileName = System.getProperty("user.dir")+"/src/test/java/resources/ZeroDisplacement.txt";

       IExpression expression = parser.buildSyntaxTree(fileName);
       Turtle turtleInterpreter = new Turtle();
       expression.interpret(turtleInterpreter);
       //parser.parseAbstractSyntaxTree(turtleInterpreter);

       Turtle expectedTurtle =  new Turtle();
       //File instructions
       double side = 10; // file instruction #size= 10
       expectedTurtle.penDown();
       expectedTurtle.move(side);
       expectedTurtle.turn(90);
       expectedTurtle.move(side);
       expectedTurtle.turn(90);
       expectedTurtle.move(side);
       expectedTurtle.turn(90);
       expectedTurtle.move(side);

       assertEquals(expectedTurtle.toString(), turtleInterpreter.toString());
   }

   @Test
    public void testInterpreterParserWithIllegalCommand(){

       InstructionParser parser = new InstructionParser();
       String fileName = System.getProperty("user.dir")+"/src/test/java/resources/IllegalCommand.txt";
       String expectedException= "Illegal Command";
       String realException = "";
       IExpression expression = null;
       try{

           expression = parser.buildSyntaxTree(fileName);
       }
       catch(IOException e){
           realException = e.getMessage();
       }
       Turtle turtleInterpreter = new Turtle();

       if( expression != null)
           expression.interpret(turtleInterpreter);

       Turtle expectedTurtle =  new Turtle();

       assertEquals(expectedException, realException);

   }

   @Test
    public void testInterpreterParserForUndeclaredConstant(){

       InstructionParser parser = new InstructionParser();
       String fileName = System.getProperty("user.dir")+"/src/test/java/resources/IllegalVariable.txt";
       String expectedException= "Illegal Constant";
       String realException = "";

       IExpression expression = null;
       try{

           expression = parser.buildSyntaxTree(fileName);
       }
       catch(IOException e){
           realException = e.getMessage();
       }

       Turtle turtleInterpreter = new Turtle();

       if( expression != null)
           expression.interpret(turtleInterpreter);


       Turtle expectedTurtle =  new Turtle();

       assertEquals(expectedException, realException);

   }

}
