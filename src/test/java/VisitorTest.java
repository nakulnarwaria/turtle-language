import TurtleImplmentation.Point;
import TurtleImplmentation.Turtle;
import interpreter.*;
import memento.TurtleMemento;
import memento.TurtleOriginator;
import org.junit.Test;
import visitor.DistanceVisitor;
import visitor.StepVisitor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class VisitorTest {

    /*
     * Tests for Step visitor
     */
    @Test
    public void testStepVisitorMementoCount() throws IOException {

        InstructionParser parser = new InstructionParser();
        String fileName = System.getProperty("user.dir")+"/src/test/java/resources/BasicFlow.txt";

        IExpression expression = parser.buildSyntaxTree(fileName);
        Turtle turtle = new Turtle();

        StepVisitor steps = new StepVisitor(turtle);

        expression.accept(steps);

        assertEquals(5,steps.getTurtleCareTaker().size());

    }

    @Test
    public void testStepVisitorMementoCountWithRepeat() throws IOException {

        InstructionParser parser = new InstructionParser();
        String fileName = System.getProperty("user.dir")+"/src/test/java/resources/RepeatWithZeroDisplacement.txt";

        IExpression expression = parser.buildSyntaxTree(fileName);
        Turtle turtle = new Turtle();

        StepVisitor steps = new StepVisitor(turtle);

        expression.accept(steps);

        assertEquals(9,steps.getTurtleCareTaker().size());
    }

    @Test
    public void testMementoCaptured() throws IOException {

        InstructionParser parser = new InstructionParser();
        String fileName = System.getProperty("user.dir")+"/src/test/java/resources/BasicFlowTwo.txt";

        IExpression expression = parser.buildSyntaxTree(fileName);
        Turtle turtle = new Turtle();

        StepVisitor steps = new StepVisitor(turtle);

        expression.accept(steps);

        Turtle expectedTurtle = new Turtle();

        List<TurtleMemento> expectedMementos = new ArrayList<TurtleMemento>();

        expectedTurtle.move(10);
        expectedMementos.add(createMemento(expectedTurtle));

        expectedTurtle.turn(90);
        expectedMementos.add(createMemento(expectedTurtle));

        expectedTurtle.move(20);
        expectedMementos.add(createMemento(expectedTurtle));

        for(int i=0;i<steps.getTurtleCareTaker().size();i++)
            assertEquals(expectedMementos.get(i).toString(),steps.getTurtleCareTaker().getTurtleMemento(i).toString());

    }

    @Test
    public void testStepVisitorMementoCapturedWithRepeat() throws IOException {

        InstructionParser parser = new InstructionParser();
        String fileName = System.getProperty("user.dir")+"/src/test/java/resources/RepeatWithZeroDisplacement.txt";

        IExpression expression = parser.buildSyntaxTree(fileName);
        Turtle turtle = new Turtle();

        StepVisitor steps = new StepVisitor(turtle);

        expression.accept(steps);

        List<TurtleMemento> expectedMemento = new ArrayList<TurtleMemento>();

        Turtle expectedTurtle = new Turtle();
        expectedTurtle.penDown();
        expectedMemento.add(createMemento(expectedTurtle));
        for(int i=0;i<4;i++){
            expectedTurtle.move(10);
            expectedMemento.add(createMemento(expectedTurtle));
            expectedTurtle.turn(90);
            expectedMemento.add(createMemento(expectedTurtle));
        }

        for(int i=0;i<steps.getTurtleCareTaker().size();i++)
            assertEquals(expectedMemento.get(i).toString(),steps.getTurtleCareTaker().getTurtleMemento(i).toString());
    }

    // Helper method for other tests
    private TurtleMemento createMemento(Turtle turtle) {

        Point location = new Point(turtle.getLocation().getX(),turtle.getLocation().getY());
        TurtleOriginator originator = new TurtleOriginator(turtle.getDirection(),turtle.getPenState(),location);
        return originator.createMemento();

    }

    /*
     * Tests for Distance visitor
     */
    @Test
    public void testDistanceVisitorDistance() throws IOException {

        InstructionParser parser = new InstructionParser();
        String fileName = System.getProperty("user.dir")+"/src/test/java/resources/BasicFlow.txt";

        IExpression expression = parser.buildSyntaxTree(fileName);
        Turtle turtle = new Turtle();

        DistanceVisitor distanceVisitor = new DistanceVisitor(turtle);

        expression.accept(distanceVisitor);

        assertEquals(45,distanceVisitor.getDistanceCovered(),0);

    }

    @Test
    public void testDistanceVisitorWithZeroDisplacement() throws IOException {


        InstructionParser parser = new InstructionParser();
        String fileName = System.getProperty("user.dir")+"/src/test/java/resources/ZeroDisplacement.txt";

        IExpression expression = parser.buildSyntaxTree(fileName);
        Turtle turtle = new Turtle();
        Point startingLocation = new Point(turtle.getLocation().getX(),turtle.getLocation().getY());

        DistanceVisitor distanceVisitor = new DistanceVisitor(turtle);

        expression.accept(distanceVisitor);

        assertEquals(60,distanceVisitor.getDistanceCovered(),0);

        //Comparing starting location's x and y coordinate to turtle's current x and y coordinate to ensure 0 displacement
        assertEquals(startingLocation.getX(),turtle.getLocation().getX(),0.1);
        assertEquals(startingLocation.getY(),turtle.getLocation().getY(),0.1);

    }

    @Test
    public void testDistanceVisistorWithRepeat() throws IOException {

        InstructionParser parser = new InstructionParser();
        String fileName = System.getProperty("user.dir")+"/src/test/java/resources/RepeatWithZeroDisplacement.txt";

        IExpression expression = parser.buildSyntaxTree(fileName);
        Turtle turtle = new Turtle();

        DistanceVisitor distanceVisitor = new DistanceVisitor(turtle);

        expression.accept(distanceVisitor);

        assertEquals(40,distanceVisitor.getDistanceCovered(),0);
    }



}
